import sqlite3


class Base:
    """
    Your methods for working with the database should be implemented in this
    class.
    """

    def __init__(self):
        self.error = ""
        self.connection = sqlite3.connect("Model/database.db", check_same_thread=False)
        try:
            self.connection.execute("""CREATE TABLE IF NOT EXISTS  persona (
                                      id integer primary key autoincrement,
                                      nombres text,
                                      correo text unique,
                                      telefono integer
                                )""")

        except sqlite3.OperationalError:
            self.connection.close()

    def count_rows(self) -> int:
        cur = self.connection.cursor()
        cur.execute('SELECT COUNT(*) from persona')
        cur_result = cur.fetchone()
        rows = cur_result[0]
        return rows

    def insert(self, data: dict) -> bool:
        try:
            self.connection.execute("insert into persona(nombres, correo, telefono) values (?,?,?)",
                                    (data["nombres"], data["correo"], data["telefono"]))
            self.connection.commit()
            return True
        except sqlite3.Error as er:
            self.error = er
            return False

    def get_user(self) -> list:
        users = []
        i = 0
        all_users = self.connection.execute("SELECT nombres, correo, telefono from persona")
        for user in all_users.fetchall():
            new_user = (i+1, user[0], user[1], user[2])
            users.append(new_user)
            i += 1
        return users

    def update(self, name, email, phone) -> bool:
        try:
            self.connection.execute("UPDATE persona SET nombres = ?, correo = ?, telefono = ? WHERE correo=?", (name, email, phone, email))
            self.connection.commit()
            return True
        except sqlite3.Error as er:
            self.error = er
            return False

    def delete(self, email) -> bool:
        try:
            self.connection.execute("DELETE FROM persona WHERE correo=?", [email])
            self.connection.commit()
            return True
        except sqlite3.Error as er:
            self.error = er
            return False

