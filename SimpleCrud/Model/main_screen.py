from Model.base_model import BaseScreenModel


class MainScreenModel(BaseScreenModel):
    """
    Implements the logic of the
    :class:`~View.main_screen.MainScreen.MainScreenView` class.
    """

    def __init__(self, base):
        self.base = base
        self.user_data = {}
        self._observers = []
        self.personas = []
        self.count = 0
        self.data = self.base.get_user()

    def create(self):
        self.count = self.base.count_rows()
        if self.base.insert(self.user_data):
            self.notify_observers("save")
        else:
            self.notify_observers(str(self.base.error))

    def edit(self):
        self.notify_observers("edit")

    def update(self, name, email, phone):
        if self.base.update(name, email, phone):
            self.notify_observers("update")
        else:
            self.notify_observers(str(self.base.error))

    def delete(self, email):
        self.count = self.base.count_rows()
        if self.base.delete(email):
            self.notify_observers("delete")
            self.data = self.base.get_user()
        else:
            self.notify_observers(str(self.base.error))

    def set_user_data(self, key, value):
        """Sets a dictionary of data that the user enters."""
        self.user_data[key] = value

    def notify_observers(self, option):
        """
        The method that will be called on the observer when the model changes.
        """

        for observer in self._observers:
            observer.model_is_changed(option)

    def add_observer(self, observer):
        self._observers.append(observer)

    def remove_observer(self, observer):
        self._observers.remove(observer)
