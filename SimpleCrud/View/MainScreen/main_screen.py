from kivy.metrics import dp
from kivy.clock import Clock
from kivymd.uix.datatables import MDDataTable

from View.base_screen import BaseScreenView


class MainScreenView(BaseScreenView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.position = 0
        self.data_tables = MDDataTable(
            size_hint=(0.7, 1),
            use_pagination=True,

            # name column, width column, sorting function column(optional)
            column_data=[
                ("No.", dp(15)),
                ("NOMBRES", dp(45)),
                ("CORREO", dp(45)),
                ("TELEFONO", dp(30)),
            ],

            row_data=self.model.data,
        )
        self.data_tables.bind(on_row_press=self.on_row_press)
        self.ids.layout.add_widget(self.data_tables)

    def on_row_press(self, instance_table, instance_row) -> None:
        '''Called when a table row is clicked.'''
        start_index, end_index = instance_row.table.recycle_data[instance_row.index]["range"]
        index = instance_row.index
        cols_num = len(instance_table. column_data)
        row_num = int(index/cols_num)
        self.position = instance_row.table.recycle_data[start_index]["text"]
        cell_row = instance_table.table_data.view_adapter.get_visible_view(row_num*cols_num)
        if cell_row.ids.check.state == 'normal':
            instance_table.table_data.select_all('normal')
            # cell_row.ids.check.state = 'down'
            cell_row.change_check_state_no_notify("down")
            self.ids.btn_save.disabled = True
            self.ids.btn_update.disabled = True
            self.ids.fullname.disabled = True
            self.ids.email.disabled = True
            self.ids.phone.disabled = True
            # populate form with selected record
            self.ids.fullname.text = instance_row.table.recycle_data[start_index+1]["text"]
            self.ids.email.text = instance_row.table.recycle_data[start_index+2]["text"]
            self.ids.phone.text = instance_row.table.recycle_data[start_index+3]["text"]
        else:
            # cell_row.ids.check.state = 'normal'
            cell_row.change_check_state_no_notify("normal")
            self.ids.btn_save.disabled = False
            self.ids.btn_update.disabled = False
            self.ids.btn_delete.disabled = False
            self.ids.fullname.disabled = False
            self.ids.email.disabled = False
            self.ids.phone.disabled = False
            self.clear_form()
        instance_table.table_data.on_mouse_select(instance_row)

    '''def check_press(self, instance_table, current_row):
        #Called when a table row is clicked.
        instance_table.unbind(on_row_press=self.on_row_press)
        Clock.schedule_once(self.binding_event_data_table_row_press, 0.5)

    def binding_event_data_table_row_press(self, dt=0):
        self.data_tables.bind(on_row_press=self.on_row_press)'''

    def model_is_changed(self, option) -> None:
        """
        Called whenever any change has occurred in the data model.
        The view in this method tracks these changes and updates the UI
        according to these changes.
        """
        if option == "save":
            self.add_row()

        elif option == "delete":
            self.remove_row()

        elif option == "edit":
            self.edit_row()

        elif option == "update":
            self.update_row()
        else:
            self.ids.mess.text = option

    def add_row(self) -> None:
        self.data_tables.add_row(
            (str(self.model.count + 1), self.ids.fullname.text, self.ids.email.text, self.ids.phone.text))
        self.clear_form()

    def remove_row(self) -> None:
        print(self.position)
        if len(self.data_tables.row_data) >= 1:
            self.data_tables.remove_row(self.data_tables.row_data[int(self.position)-1])
            self.clear_form()
            self.data_tables.row_data = self.model.data

    def update_row(self):
        self.data_tables.update_row(
            self.data_tables.row_data[int(self.position)-1],  # old row data
            [self.position, self.ids.fullname.text, self.ids.email.text, self.ids.phone.text],          # new row data
        )

    def edit_row(self):
        self.ids.fullname.disabled = False
        self.ids.email.disabled = False
        self.ids.phone.disabled = False
        self.ids.btn_update.disabled = False
        self.ids.btn_delete.disabled = True

    def clear_form(self):
        self.ids.num.text = ""
        self.ids.fullname.text = ""
        self.ids.email.text = ""
        self.ids.phone.text = ""
